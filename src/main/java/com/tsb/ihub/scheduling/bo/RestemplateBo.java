package com.tsb.ihub.scheduling.bo;

import com.tsb.ihub.scheduling.constant.DtoConst;

public class RestemplateBo {

	DtoConst dtoConst;
	
	String url;

	Object reqObj;

	Object resObj;
	
	public DtoConst getDtoConst() {
		return dtoConst;
	}

	public void setDtoConst(DtoConst dtoConst) {
		this.dtoConst = dtoConst;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Object getReqObj() {
		return reqObj;
	}

	public void setReqObj(Object reqObj) {
		this.reqObj = reqObj;
	}

	public Object getResObj() {
		return resObj;
	}

	public void setResObj(Object resObj) {
		this.resObj = resObj;
	}

}
