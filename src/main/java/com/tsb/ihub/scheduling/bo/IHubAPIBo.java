package com.tsb.ihub.scheduling.bo;

import java.util.Date;

public class IHubAPIBo {

	private String sessionId;

	private String subTxSn;

	private String subTxId;

	private String subType;

	private String msgType;

	private String msg;

	private Date msgTime;

	private String subRtnCode;

	private String subRtnMsg;

	private String extension;
	
	private RestemplateBo restemplateBo;
	
	private Exception exception;

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public String getSubTxSn() {
		return subTxSn;
	}

	public void setSubTxSn(String subTxSn) {
		this.subTxSn = subTxSn;
	}

	public String getSubTxId() {
		return subTxId;
	}

	public void setSubTxId(String subTxId) {
		this.subTxId = subTxId;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public String getMsgType() {
		return msgType;
	}

	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public Date getMsgTime() {
		return msgTime;
	}

	public void setMsgTime(Date msgTime) {
		this.msgTime = msgTime;
	}

	public String getSubRtnCode() {
		return subRtnCode;
	}

	public void setSubRtnCode(String subRtnCode) {
		this.subRtnCode = subRtnCode;
	}

	public String getSubRtnMsg() {
		return subRtnMsg;
	}

	public void setSubRtnMsg(String subRtnMsg) {
		this.subRtnMsg = subRtnMsg;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public RestemplateBo getRestemplateBo() {
		return restemplateBo;
	}

	public void setRestemplateBo(RestemplateBo restemplateBo) {
		this.restemplateBo = restemplateBo;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}
}
