package com.tsb.ihub.scheduling.scheduler;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.tsb.ihub.scheduling.service.IhubSchduleService;

@Component
public class EjLogScheduler {

	@Autowired
	private IhubSchduleService ihubSchduleService;

	@Scheduled(cron = "*/3 * * * * *")
	public void printEjLog() {
		ihubSchduleService.excuteEjLogSchedule();
	}

}
