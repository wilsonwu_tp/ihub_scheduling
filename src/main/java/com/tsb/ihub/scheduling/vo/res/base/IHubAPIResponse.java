package com.tsb.ihub.scheduling.vo.res.base;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IHubAPIResponse<T extends IHubAPIResBody> {

	@JsonProperty("header")
	private IHubAPIResHeader iHubAPIResHeader;
	
	@JsonProperty("body")
	private T body;

	public IHubAPIResHeader getiHubAPIResHeader() {
		return iHubAPIResHeader;
	}

	public void setiHubAPIResHeader(IHubAPIResHeader iHubAPIResHeader) {
		this.iHubAPIResHeader = iHubAPIResHeader;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}
	
	
}
