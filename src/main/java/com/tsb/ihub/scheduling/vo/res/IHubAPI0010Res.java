package com.tsb.ihub.scheduling.vo.res;

import com.tsb.ihub.scheduling.vo.res.base.IHubAPIResBody;

public class IHubAPI0010Res extends IHubAPIResBody {
	
	private String rtnCode;
	
	private String rtnMsg;

	public String getRtnCode() {
		return rtnCode;
	}

	public void setRtnCode(String rtnCode) {
		this.rtnCode = rtnCode;
	}

	public String getRtnMsg() {
		return rtnMsg;
	}

	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}
	
	

}
