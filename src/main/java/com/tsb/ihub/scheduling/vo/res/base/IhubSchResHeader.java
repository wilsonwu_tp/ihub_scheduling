package com.tsb.ihub.scheduling.vo.res.base;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IhubSchResHeader {

	@JsonProperty("rtnCode")
	String rtnCode;

	@JsonProperty("rtnMsg")
	String rtnMsg;

	public String getRtnCode() {
		return rtnCode;
	}

	public void setRtnCode(String rtnCode) {
		this.rtnCode = rtnCode;
	}

	public String getRtnMsg() {
		return rtnMsg;
	}

	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}

}
