package com.tsb.ihub.scheduling.vo.req.base;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IhubSchRequest<T extends IhubSchReqBody> {

	@JsonProperty("header")
	private IhubSchReqHeader ihubSchReqHeader;
	
	
	@JsonProperty("body")
	private T body;


	public IhubSchReqHeader getIhubSchReqHeader() {
		return ihubSchReqHeader;
	}


	public void setIhubSchReqHeader(IhubSchReqHeader ihubSchReqHeader) {
		this.ihubSchReqHeader = ihubSchReqHeader;
	}


	public T getBody() {
		return body;
	}


	public void setBody(T body) {
		this.body = body;
	}
	
	
}
