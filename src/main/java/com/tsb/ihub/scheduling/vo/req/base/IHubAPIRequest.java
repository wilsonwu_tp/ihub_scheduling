package com.tsb.ihub.scheduling.vo.req.base;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tsb.ihub.scheduling.vo.res.base.IHubAPIResBody;

public class IHubAPIRequest<T extends IHubAPIReqBody> {

	@JsonProperty("header")
	private IHubAPIReqHeader iHubAPIReqHeader;
	
	
	@JsonProperty("body")
	private T body;


	public IHubAPIReqHeader getiHubAPIReqHeader() {
		return iHubAPIReqHeader;
	}


	public void setiHubAPIReqHeader(IHubAPIReqHeader iHubAPIReqHeader) {
		this.iHubAPIReqHeader = iHubAPIReqHeader;
	}


	public T getBody() {
		return body;
	}


	public void setBody(T body) {
		this.body = body;
	}
	
}
