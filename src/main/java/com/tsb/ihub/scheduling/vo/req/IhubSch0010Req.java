package com.tsb.ihub.scheduling.vo.req;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tsb.ihub.scheduling.vo.req.base.IhubSchReqBody;

public class IhubSch0010Req extends IhubSchReqBody {

	@JsonProperty("reqMsg")
	private String reqMsg;

	public String getReqMsg() {
		return reqMsg;
	}

	public void setReqMsg(String reqMsg) {
		this.reqMsg = reqMsg;
	}


	
}
