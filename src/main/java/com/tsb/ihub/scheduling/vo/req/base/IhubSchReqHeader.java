package com.tsb.ihub.scheduling.vo.req.base;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IhubSchReqHeader {

	@JsonProperty("txDate")
	private String txDate;

	@JsonProperty("txID")
	private String txID;

	@JsonProperty("cID")
	private String cID;

	public String getTxDate() {
		return txDate;
	}

	public void setTxDate(String txDate) {
		this.txDate = txDate;
	}

	public String getTxID() {
		return txID;
	}

	public void setTxID(String txID) {
		this.txID = txID;
	}

	public String getcID() {
		return cID;
	}

	public void setcID(String cID) {
		this.cID = cID;
	}

}
