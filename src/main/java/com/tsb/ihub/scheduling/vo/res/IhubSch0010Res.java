package com.tsb.ihub.scheduling.vo.res;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.tsb.ihub.scheduling.vo.res.base.IhubSchResBody;

public class IhubSch0010Res extends IhubSchResBody{

	@JsonProperty("rtnMsgDescription")
	private String rtnMsgDescription;

	public String getRtnMsgDescription() {
		return rtnMsgDescription;
	}

	public void setRtnMsgDescription(String rtnMsgDescription) {
		this.rtnMsgDescription = rtnMsgDescription;
	}
	
	
	
}
