package com.tsb.ihub.scheduling.vo.res.base;

import com.fasterxml.jackson.annotation.JsonProperty;

public class IhubSchResponse<T extends IhubSchResBody> {

	
	@JsonProperty("header")
	private IhubSchResHeader ihubSchResHeader;
	
	@JsonProperty("body")
	private T body;
	
	public IhubSchResponse() {	
	}
	
	public IhubSchResponse(IhubSchResHeader ihubSchResHeader) {
		super();
		this.ihubSchResHeader = ihubSchResHeader;
	}

	public IhubSchResHeader getIhubSchResHeader() {
		return ihubSchResHeader;
	}

	public void setIhubSchResHeader(IhubSchResHeader ihubSchResHeader) {
		this.ihubSchResHeader = ihubSchResHeader;
	}

	public T getBody() {
		return body;
	}

	public void setBody(T body) {
		this.body = body;
	}
	
	
	
}
