package com.tsb.ihub.scheduling.service;

import javax.servlet.http.HttpServletRequest;

import com.tsb.ihub.scheduling.vo.req.base.IHubAPIRequest;
import com.tsb.ihub.scheduling.vo.res.base.IHubAPIResponse;

public interface CallIHubAPIService {
	
	public IHubAPIResponse<?> callIHubAPIRequest(String url,IHubAPIRequest<?> request,HttpServletRequest httpRequest) throws Exception;

}
