package com.tsb.ihub.scheduling.service.impl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tsb.ihub.scheduling.bo.IHubAPIBo;
import com.tsb.ihub.scheduling.bo.RestemplateBo;
import com.tsb.ihub.scheduling.constant.DtoConst;
import com.tsb.ihub.scheduling.service.CallIHubAPIService;
import com.tsb.ihub.scheduling.service.SendIHubAPIService;
import com.tsb.ihub.scheduling.util.JsonUtil;
import com.tsb.ihub.scheduling.vo.req.base.IHubAPIRequest;
import com.tsb.ihub.scheduling.vo.res.base.IHubAPIResponse;

@Service
public class CallIHubAPIServiceImpl implements CallIHubAPIService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CallIHubAPIService.class);
	
	@Autowired
	SendIHubAPIService sendIHubAPIService;

	@Override
	public IHubAPIResponse<?> callIHubAPIRequest(String url, IHubAPIRequest<?> request,HttpServletRequest httpRequest) throws Exception {
	
		RestemplateBo restemplateBo = getRestemplateBo(url, DtoConst.IHUBAPIBO_TO_EJLOG, request);
		IHubAPIBo iHubAPIBoReq = getIHubAPIBo(url, request, httpRequest.getSession().getId(), restemplateBo);
		
		IHubAPIBo ihubApiBoRes = sendIHubAPIService.sendIHubAPIBo(iHubAPIBoReq);
		LOGGER.info("call  send  ihubapi Msg "+ihubApiBoRes.getMsg());
		
		LOGGER.info("IHubAPIBoRes : {} ",JsonUtil.objectToJsonNoNull(ihubApiBoRes));
		
		return new IHubAPIResponse<>();
	}
	private IHubAPIBo getIHubAPIBo(String url,IHubAPIRequest<?> request,String sessionId,RestemplateBo restemplateBo) throws Exception {
		
		IHubAPIBo iHubAPIBoReq  = new IHubAPIBo();
		String json = JsonUtil.objectToJsonNoNull(request);
		iHubAPIBoReq.setSessionId(sessionId);
		iHubAPIBoReq.setSubTxSn("testSubTxSn");
		iHubAPIBoReq.setSubTxId("testSubTxId");
		iHubAPIBoReq.setSubType("testSubType");
		iHubAPIBoReq.setMsgType("1");
		iHubAPIBoReq.setMsg(json);
		iHubAPIBoReq.setMsgTime(new Date());
		iHubAPIBoReq.setSubRtnCode("req");
		iHubAPIBoReq.setSubRtnMsg("C0000");
		iHubAPIBoReq.setExtension("req success");
		iHubAPIBoReq.setRestemplateBo(restemplateBo);
		
		return iHubAPIBoReq;
	}

	private RestemplateBo getRestemplateBo(String url,DtoConst dtoConst,Object iHubReq) {
		RestemplateBo restemplateBo = new RestemplateBo();
		restemplateBo.setDtoConst(dtoConst);
		restemplateBo.setReqObj(iHubReq);
		restemplateBo.setUrl(url);

		return restemplateBo;
	}
	
}
