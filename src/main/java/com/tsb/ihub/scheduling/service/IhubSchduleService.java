package com.tsb.ihub.scheduling.service;

import javax.servlet.http.HttpServletRequest;

import com.tsb.ihub.scheduling.vo.req.IHubAPI0010Req;
import com.tsb.ihub.scheduling.vo.req.IhubSch0010Req;
import com.tsb.ihub.scheduling.vo.req.base.IHubAPIRequest;
import com.tsb.ihub.scheduling.vo.req.base.IhubSchRequest;
import com.tsb.ihub.scheduling.vo.res.IHubAPI0010Res;
import com.tsb.ihub.scheduling.vo.res.IhubSch0010Res;
import com.tsb.ihub.scheduling.vo.res.base.IHubAPIResponse;
import com.tsb.ihub.scheduling.vo.res.base.IhubSchResponse;

public interface IhubSchduleService {
	
	//測試連接DB
	IhubSchResponse<IhubSch0010Res> testSaveEjLog(IhubSchRequest<IhubSch0010Req> request) throws Exception;
	
	//印出ejLog
	void excuteEjLogSchedule();
	
	//測試restemplate ejLog
	IHubAPIResponse<IHubAPI0010Res> testRestemplateEjLog(IHubAPIRequest<IHubAPI0010Req> request ,HttpServletRequest httpRequest) throws Exception;
}
