package com.tsb.ihub.scheduling.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import com.tsb.ihub.scheduling.constant.DtoConst;
import com.tsb.ihub.scheduling.dao.EjLogDAO;
import com.tsb.ihub.scheduling.po.EjLogPo;
import com.tsb.ihub.scheduling.service.LogService;
import com.tsb.ihub.scheduling.util.JsonUtil;

@Service
public class LogServiceImpl implements LogService{
	
	private static final Logger LOGGER = LoggerFactory.getLogger(LogService.class);
	
	@Autowired
	private EjLogDAO ejLogDao;
	
	@Async("ejLogExecutor")
	@Override
	public void writeEjLog(Object obj, DtoConst dtoConst) throws Exception {
		EjLogPo ejLogPo = (EjLogPo)dtoConst.getDoTransfer().apply(obj);
		ejLogDao.save(ejLogPo);
		
	}

}
