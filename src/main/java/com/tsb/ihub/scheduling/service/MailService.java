package com.tsb.ihub.scheduling.service;

import com.tsb.ihub.scheduling.bo.MailBo;

public interface MailService {
	
	   public void send(MailBo mailBo) throws Exception;

}
