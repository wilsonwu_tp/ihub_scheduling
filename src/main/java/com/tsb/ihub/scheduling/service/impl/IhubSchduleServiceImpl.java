package com.tsb.ihub.scheduling.service.impl;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.tsb.ihub.scheduling.bo.IHubAPIBo;
import com.tsb.ihub.scheduling.bo.MailBo;
import com.tsb.ihub.scheduling.bo.RestemplateBo;
import com.tsb.ihub.scheduling.constant.DtoConst;
import com.tsb.ihub.scheduling.dao.EjLogDAO;
import com.tsb.ihub.scheduling.po.EjLogPo;
import com.tsb.ihub.scheduling.service.CallIHubAPIService;
import com.tsb.ihub.scheduling.service.IhubSchduleService;
import com.tsb.ihub.scheduling.service.MailService;
import com.tsb.ihub.scheduling.service.SendIHubAPIService;
import com.tsb.ihub.scheduling.vo.req.IHubAPI0010Req;
import com.tsb.ihub.scheduling.vo.req.IhubSch0010Req;
import com.tsb.ihub.scheduling.vo.req.base.IHubAPIRequest;
import com.tsb.ihub.scheduling.vo.req.base.IhubSchRequest;
import com.tsb.ihub.scheduling.vo.res.IHubAPI0010Res;
import com.tsb.ihub.scheduling.vo.res.IhubSch0010Res;
import com.tsb.ihub.scheduling.vo.res.base.IHubAPIResponse;
import com.tsb.ihub.scheduling.vo.res.base.IhubSchResHeader;
import com.tsb.ihub.scheduling.vo.res.base.IhubSchResponse;

@Service
public class IhubSchduleServiceImpl implements IhubSchduleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(IhubSchduleService.class);

	@Autowired
	EjLogDAO ejLogDao;

	@Value("${properties.log}")
	private String propertieScheduler;

	@Autowired
	SendIHubAPIService sendIHubAPIService;

	@Autowired
	CallIHubAPIService callIHubAPIService;

	@Autowired
	MailService mailService;


	@Override
	public IhubSchResponse<IhubSch0010Res> testSaveEjLog(IhubSchRequest<IhubSch0010Req> request) throws Exception {

		IhubSchResponse<IhubSch0010Res> response = setIhubSch();
//		saveEjLog();
		MailBo mailBo = new MailBo();
		mailService.send(mailBo);
		return response;
	}

	@Override
	public void excuteEjLogSchedule() {
		LOGGER.info(" print ejLog schedule");
		LOGGER.info("---------" + propertieScheduler);
	}

	@Override
	public IHubAPIResponse<IHubAPI0010Res> testRestemplateEjLog(IHubAPIRequest<IHubAPI0010Req> request,
			HttpServletRequest httpRequest) throws Exception {

		IHubAPIRequest<IHubAPI0010Req> req = new IHubAPIRequest<IHubAPI0010Req>();

		String url = "https://isports.sa.gov.tw/Api/Rest/V1/Activity.svc/GetActivityList?county=A&activityKind=1&activityDateBegin=1070101&activityDateEnd=1080101&paging=true&pageSize=1&pageNo=1";

		callIHubAPIService.callIHubAPIRequest(url, req, httpRequest);

		return createMockResponse();
	}

	private IHubAPIResponse<IHubAPI0010Res> createMockResponse() {
		IHubAPIResponse<IHubAPI0010Res> res = new IHubAPIResponse<IHubAPI0010Res>();
		IHubAPI0010Res resbody = new IHubAPI0010Res();
		resbody.setRtnCode("C0000");
		resbody.setRtnMsg("Success");
		res.setBody(resbody);

		return res;
	}

	private void saveEjLog() {
		EjLogPo ejLogPO = new EjLogPo();
		ejLogPO.setSessionId("12345678");
		ejLogPO.setSubTxSn("tgbyhnujmik");
		ejLogPO.setSubTxId("IhubSch0010Req");
		ejLogPO.setSubType("ihub_scheduling");
		ejLogPO.setMsgType("1");
		ejLogPO.setMsg("rfbtgbyhnujm");
		ejLogPO.setMsgTime(new Date());
		ejLogPO.setMsgType("1100");
		ejLogPO.setSubRtnCode("C0001");
		ejLogPO.setSubRtnMsg("OK");
		ejLogPO.setExtension("checkGood");

		ejLogDao.save(ejLogPO);

	}

	private IhubSchResponse<IhubSch0010Res> setIhubSch() {
		IhubSchResponse<IhubSch0010Res> response = new IhubSchResponse<>();
		IhubSch0010Res ihubSch0010Res = new IhubSch0010Res();
		IhubSchResHeader ihubSchResHeader = new IhubSchResHeader();
		ihubSchResHeader.setRtnCode("C0001");
		ihubSchResHeader.setRtnMsg("OK");
		ihubSch0010Res.setRtnMsgDescription("rtn Msg ihubSch");
		response.setBody(ihubSch0010Res);
		response.setIhubSchResHeader(ihubSchResHeader);

		return response;
	}
}
