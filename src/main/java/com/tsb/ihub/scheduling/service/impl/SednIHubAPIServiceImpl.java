package com.tsb.ihub.scheduling.service.impl;

import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.ThrowsAdvice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import com.tsb.ihub.scheduling.bo.IHubAPIBo;
import com.tsb.ihub.scheduling.bo.MailBo;
import com.tsb.ihub.scheduling.bo.RestemplateBo;
import com.tsb.ihub.scheduling.constant.RtnCode;
import com.tsb.ihub.scheduling.exception.IhubScheduleException;
import com.tsb.ihub.scheduling.service.MailService;
import com.tsb.ihub.scheduling.service.SendIHubAPIService;

@Service
public class SednIHubAPIServiceImpl implements SendIHubAPIService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SendIHubAPIService.class);

	@Autowired
	RestTemplate restTemplate;
	@Autowired
	MailService mailService;

	@Override
	@Retryable(value = Exception.class, maxAttemptsExpression = "#{${send.ihubapi.retry.count}}")
	public IHubAPIBo sendIHubAPIBo(IHubAPIBo ihubAPIBo) throws Exception {
		IHubAPIBo iHubAPIBoRes = new IHubAPIBo();
		try {
			String url = ihubAPIBo.getRestemplateBo().getUrl();
			LOGGER.info("url : {} ", url);
			ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
			if (!isSuccessResponse(response.getStatusCode())) {
				throw new IhubScheduleException(RtnCode.C0097);
			}
			LOGGER.info("response : {} ", response.getBody());
			String resJson = response.getBody().toString();
			iHubAPIBoRes = createIHubAPIBo(iHubAPIBoRes, resJson);
			return iHubAPIBoRes;
		} catch (ResourceAccessException e) {
			LOGGER.info("error time out");
			throw new ResourceAccessException("錯誤");
		} catch (Exception e) {
			LOGGER.info("error");
			throw new Exception();
		}

	}

	@Recover
	public IHubAPIBo recoverTest(ResourceAccessException e) throws Exception {
		LOGGER.info("time error");
		MailBo mailBo = createMailBo();
		mailService.send(mailBo);
		IHubAPIBo ihubapibo = new IHubAPIBo();
		ihubapibo.setMsg("recover time");
		return ihubapibo;
	}
	@Recover
	public IHubAPIBo recoverTest(Exception e) throws Exception {
		LOGGER.info("error");
		IHubAPIBo ihubapibo = new IHubAPIBo();
		MailBo mailBo = createMailBo();
		mailService.send(mailBo);
		ihubapibo.setMsg("recover test");
		return ihubapibo;
	}
	
	private MailBo createMailBo() {
		MailBo mailBo = new MailBo();
		mailBo.setReceiver("wilson27561@gmail.com");
		mailBo.setSubject("主旨：發送電文失敗");
		mailBo.setText("exception mail");
		
		return mailBo;
	}

	private IHubAPIBo createIHubAPIBo(IHubAPIBo iHubAPIBoRes, String resjson) {
		iHubAPIBoRes.setSessionId("4rfvtgbyhn7ujm");
		iHubAPIBoRes.setSubTxSn("testSubTxSn");
		iHubAPIBoRes.setSubTxId("testSubTxId");
		iHubAPIBoRes.setSubType("testSubType");
		iHubAPIBoRes.setMsgType("1");
		iHubAPIBoRes.setMsg("resjsonSuccess");
		iHubAPIBoRes.setMsgTime(new Date());
		iHubAPIBoRes.setSubRtnMsg("testMsg");
		iHubAPIBoRes.setSubRtnCode("C0000");
		iHubAPIBoRes.setExtension("test Res success");
		RestemplateBo restemplateBo = new RestemplateBo();
		restemplateBo.setResObj("resjsonSuccess");

		iHubAPIBoRes.setRestemplateBo(restemplateBo);
		return iHubAPIBoRes;
	}

	private boolean isSuccessResponse(HttpStatus statusCode) {
		return HttpStatus.OK.equals(statusCode);
	}

}
