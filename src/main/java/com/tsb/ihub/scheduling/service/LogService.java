package com.tsb.ihub.scheduling.service;

import com.tsb.ihub.scheduling.constant.DtoConst;

public interface LogService {
	
	public void writeEjLog(Object obj,DtoConst dtoConst) throws Exception;

}
