package com.tsb.ihub.scheduling.service;

import com.tsb.ihub.scheduling.bo.IHubAPIBo;

public interface SendIHubAPIService {
	
	public IHubAPIBo sendIHubAPIBo(IHubAPIBo ihubAPIBo) throws Exception;
	

}
