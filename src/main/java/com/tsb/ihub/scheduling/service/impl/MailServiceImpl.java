package com.tsb.ihub.scheduling.service.impl;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.tsb.ihub.scheduling.bo.MailBo;
import com.tsb.ihub.scheduling.service.MailService;

@Service
public class MailServiceImpl implements MailService {

	@Autowired
	private JavaMailSender mailSender;

	@Async("emailExecutor")
	@Override
	public void send(MailBo mailBo) throws Exception {
		MimeMessage mimeMessage = mailSender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
		helper.setTo(mailBo.getReceiver());
		helper.setSubject(mailBo.getSubject());
		helper.setText(mailBo.getText(), true);
		mailSender.send(mimeMessage);
	}

}
