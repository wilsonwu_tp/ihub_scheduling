package com.tsb.ihub.scheduling.aop;

import java.util.stream.Stream;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.tsb.ihub.scheduling.bo.IHubAPIBo;
import com.tsb.ihub.scheduling.constant.AspectConst;
import com.tsb.ihub.scheduling.constant.DtoConst;
import com.tsb.ihub.scheduling.service.LogService;



@Component
@Aspect
public class EjLogAspect {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EjLogAspect.class);
	
	@Autowired
	LogService logService;
	
	@Around(value=AspectConst.POINTCUT_EJLOGLAYER)
	public Object handleEjLogRequest(ProceedingJoinPoint joinPoint) throws Throwable{
		LOGGER.info(" write EjLog ");
		
		Object[] objsReq =joinPoint.getArgs();
		
		Stream.of(objsReq).forEach(obj->{
			if(obj instanceof IHubAPIBo) {
				try {
					logService.writeEjLog(obj, DtoConst.IHUBAPIBO_TO_EJLOG);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		Object result = joinPoint.proceed();
		
		Stream.of(result).forEach(obj->{
			if(obj instanceof IHubAPIBo) {
				try {
					logService.writeEjLog(obj, DtoConst.IHUBAPIBO_TO_EJLOG);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
		return null;
	}

}
