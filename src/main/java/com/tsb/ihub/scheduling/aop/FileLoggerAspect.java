package com.tsb.ihub.scheduling.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.tsb.ihub.scheduling.constant.AspectConst;

@Aspect
@Component
public class FileLoggerAspect {

	private static final Logger LOGGER = LoggerFactory.getLogger(FileLoggerAspect.class);

	@Around(value = AspectConst.POINTCUT_SERVICELAYER)
	public Object logAroundServiceLayer(ProceedingJoinPoint jp) throws Throwable {

		Object result;
		String targetClassName = jp.getTarget().getClass().getSimpleName();
		String signatureName = jp.getSignature().getName();
		String methodName = String.format("%s:%s ", targetClassName, signatureName);

		LOGGER.info("{} () Start...", methodName);

		result = jp.proceed();

		LOGGER.info("{} () return End...", methodName);

		return result;

	}
}
