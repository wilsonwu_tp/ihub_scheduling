package com.tsb.ihub.scheduling.aop.pointcut;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class PointcutDefinition {

	@Pointcut("within(@org.springframework.stereotype.Service *)")
	public void serviceLayer() {};
	
	@Pointcut("execution(* com.tsb.ihub.scheduling.service.SendIHubAPIService.*(..))")
	public void ejLogLayer() {};
}
