package com.tsb.ihub.scheduling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.retry.annotation.EnableRetry;


/**
 * Item
 * a.Spring Scheduler 
 * b.Spring Data + H2 DB
 * c.Spring AOP
 * d.Restemplate
 * e.Restful API
 * f.Exception Handler
 * g.Async EJLog
 * h.Async Exception Mail
 * i.Spring Retry 
 * j.get properties by point cofig $java -jar -Dspring.config.location=/Users/wilson27561/Documents/jar/config/config.properties ihub_scheduling-0.1.06.15.05.jar
 * @author wilson27561
 *
 */
@SpringBootApplication
@EnableRetry
public class IhubSchedulingApplication {

	public static void main(String[] args) {
		SpringApplication.run(IhubSchedulingApplication.class, args);
						
	}

}
