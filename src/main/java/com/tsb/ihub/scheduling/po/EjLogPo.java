package com.tsb.ihub.scheduling.po;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;



@Entity
@Table(name="EJ_LOG")
public class EjLogPo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE ,generator = "ejlog_seq")
	@SequenceGenerator(sequenceName="EJ_LOG_SEQ", allocationSize=1,name = "ejlog_seq")
	@Column(name="EL_SEQ",nullable=false)
	private BigDecimal elSeq;
	
	@Column(name="SESSION_ID",nullable=false)
	private String sessionId;
	
	@Column(name="SUB_TX_SN",nullable=false)
	private String subTxSn;
	
	@Column(name="SUB_TX_ID")
	private String subTxId;
	
	@Column(name="SUB_TYPE")
	private String subType;
	
	@Column(name="MSG_TYPE")
	private String msgType;
	
	@Column(name="MSG")
	private String msg;
	
	@Column(name="MSG_TIME",nullable=false)
	private Date msgTime;
	
	@Column(name="SUB_RTN_CODE",nullable=false)
	private String subRtnCode;
	
	@Column(name="SUB_RTN_MSG",nullable=false)
	private String subRtnMsg;
	
	@Column(name="EXTENSION")
	private String extension;
	
	
	public BigDecimal getElSeq() {
		return elSeq;
	}
	public void setElSeq(BigDecimal elSeq) {
		this.elSeq = elSeq;
	}
	public String getSessionId() {
		return sessionId;
	}
	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}
	public String getSubTxSn() {
		return subTxSn;
	}
	public void setSubTxSn(String subTxSn) {
		this.subTxSn = subTxSn;
	}
	public String getSubTxId() {
		return subTxId;
	}
	public void setSubTxId(String subTxId) {
		this.subTxId = subTxId;
	}
	public String getSubType() {
		return subType;
	}
	public void setSubType(String subType) {
		this.subType = subType;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	public String getMsg() {
		return msg;
	}
	public void setMsg(String msg) {
		this.msg = msg;
	}
	public Date getMsgTime() {
		return msgTime;
	}
	public void setMsgTime(Date msgTime) {
		this.msgTime = msgTime;
	}
	public String getSubRtnCode() {
		return subRtnCode;
	}
	public void setSubRtnCode(String subRtnCode) {
		this.subRtnCode = subRtnCode;
	}
	public String getSubRtnMsg() {
		return subRtnMsg;
	}
	public void setSubRtnMsg(String subRtnMsg) {
		this.subRtnMsg = subRtnMsg;
	}
	public String getExtension() {
		return extension;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}

}
