package com.tsb.ihub.scheduling.exception;

import com.tsb.ihub.scheduling.constant.RtnCode;

public class IhubScheduleException extends RuntimeException{
	
	private RtnCode code;
	private String[] args;

	public IhubScheduleException(RtnCode rtnCode) {
		super(rtnCode.getRtnMsg());
		this.code = rtnCode;
	}
	public IhubScheduleException(RtnCode rtnCode,String[] args) {
		this.code = rtnCode;
		this.args = args;
	}
	
	public static IhubScheduleException createByErrCode(RtnCode rtnCode) {
		return new IhubScheduleException(rtnCode);
	}
	public static IhubScheduleException createByErrCodeAndArgs(RtnCode rtnCode,String[] args) {
		return new IhubScheduleException(rtnCode,args);
	}
	
	public RtnCode getCode() {
		return code;
	}
	public void setCode(RtnCode code) {
		this.code = code;
	}
	public String[] getArgs() {
		return args;
	}
	public void setArgs(String[] args) {
		this.args = args;
	}
}
