package com.tsb.ihub.scheduling.handler;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.tsb.ihub.scheduling.constant.RtnCode;
import com.tsb.ihub.scheduling.exception.IhubScheduleException;
import com.tsb.ihub.scheduling.vo.res.base.IhubSchResHeader;
import com.tsb.ihub.scheduling.vo.res.base.IhubSchResponse;

@ControllerAdvice
public class ExceptionHandle {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandle.class);
	
	@ExceptionHandler(IhubScheduleException.class)
	public ResponseEntity<Object> IhubSchedulingExceptionHandle(IhubScheduleException ex,HttpServletRequest request){
		LOGGER.error("Get ihubScheduling error ",ex);
		IhubSchResHeader ihubSchResHeader = new IhubSchResHeader();
		ihubSchResHeader.setRtnCode(ex.getCode().getRtnCode());
		LOGGER.info("errorMsg : {} ",ex.getCode().getRtnMsg());
		ihubSchResHeader.setRtnMsg(MessageFormat.format(ex.getCode().getRtnMsg(), ex.getArgs()));

		return ResponseEntity.status(HttpStatus.OK).body(new IhubSchResponse<>(ihubSchResHeader));
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<Object> GenralExceptionHandle(Exception e,HttpServletRequest request){
		LOGGER.error("genreal ExceptionHandler error ");
		
		LOGGER.error("Exception  ",e);
		IhubSchResHeader ihubSchResHeader = new IhubSchResHeader();
		ihubSchResHeader.setRtnCode(RtnCode.C0099.getRtnCode());
		ihubSchResHeader.setRtnMsg(RtnCode.C0099.getRtnMsg());

		return ResponseEntity.status(HttpStatus.OK).body(new IhubSchResponse<>(ihubSchResHeader));
	}
	

}
