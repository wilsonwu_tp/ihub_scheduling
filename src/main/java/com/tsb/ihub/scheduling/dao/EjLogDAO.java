package com.tsb.ihub.scheduling.dao;

import java.math.BigDecimal;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tsb.ihub.scheduling.po.EjLogPo;

@Repository
public interface EjLogDAO extends CrudRepository<EjLogPo, BigDecimal>{


}
