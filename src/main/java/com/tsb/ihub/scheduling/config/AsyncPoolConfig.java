package com.tsb.ihub.scheduling.config;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
@EnableAsync
public class AsyncPoolConfig {
	
	@Bean("emailExecutor")
	public Executor emailExecutor() {
		ThreadPoolTaskExecutor executor = new AsyncBaseConfig().defaultCofig("email-",20,1000);
		return executor;
	}
	
	@Bean("ejLogExecutor")
	public Executor ejLogExecutor() {
		ThreadPoolTaskExecutor executor = new AsyncBaseConfig().defaultCofig("ej-log-",20,1000);
		return executor;
	}
	

}
