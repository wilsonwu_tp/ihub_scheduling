package com.tsb.ihub.scheduling.config;

import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

public class AsyncBaseConfig {
	
	public ThreadPoolTaskExecutor defaultCofig(String namePrefix,int poolSize,int queueSize) {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(poolSize);
		executor.setMaxPoolSize(poolSize);
		executor.setQueueCapacity(queueSize);
		executor.setKeepAliveSeconds(60);
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		executor.setWaitForTasksToCompleteOnShutdown(true);
		executor.setAwaitTerminationSeconds(5);
		executor.setThreadNamePrefix(namePrefix);
	
		return executor;
	}
	
	public ThreadPoolTaskExecutor defaultCofig(String namePrefix) {
		return defaultCofig(namePrefix, 10, 100);
	}

}
