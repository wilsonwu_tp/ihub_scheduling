package com.tsb.ihub.scheduling.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tsb.ihub.scheduling.service.IhubSchduleService;
import com.tsb.ihub.scheduling.vo.req.IHubAPI0010Req;
import com.tsb.ihub.scheduling.vo.req.IhubSch0010Req;
import com.tsb.ihub.scheduling.vo.req.base.IHubAPIRequest;
import com.tsb.ihub.scheduling.vo.req.base.IhubSchRequest;
import com.tsb.ihub.scheduling.vo.res.IHubAPI0010Res;
import com.tsb.ihub.scheduling.vo.res.IhubSch0010Res;
import com.tsb.ihub.scheduling.vo.res.base.IHubAPIResponse;
import com.tsb.ihub.scheduling.vo.res.base.IhubSchResponse;

@RestController
@RequestMapping("/ihub")
public class TestController {
	
	@Autowired
	IhubSchduleService ihubSchduleService;
	
	/**
	 * @param request
	 * @return
	 * @throws Exception
	 */
	@RequestMapping(value="/sch",method = RequestMethod.POST)
	public @ResponseBody IhubSchResponse<IhubSch0010Res> testSch(@RequestBody IhubSchRequest<IhubSch0010Req> request) throws Exception{
		return ihubSchduleService.testSaveEjLog(request);
	}
	
	@RequestMapping(value="/api",method = RequestMethod.POST)
	public @ResponseBody IHubAPIResponse<IHubAPI0010Res> testApi(@RequestBody IHubAPIRequest<IHubAPI0010Req> request,HttpServletRequest httpRequest) throws Exception{
		return ihubSchduleService.testRestemplateEjLog(request, httpRequest);
	}

	
	

}
