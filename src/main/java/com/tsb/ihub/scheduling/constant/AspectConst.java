package com.tsb.ihub.scheduling.constant;

public class AspectConst {
	
	private static final String POINTCUT_DEFINITION_PATH = "com.tsb.ihub.scheduling.aop.pointcut.PointcutDefinition.";
	
	public static final String  POINTCUT_EJLOGLAYER = POINTCUT_DEFINITION_PATH + "ejLogLayer()";
	
	public static final String  POINTCUT_SERVICELAYER = POINTCUT_DEFINITION_PATH + "serviceLayer()";

}
