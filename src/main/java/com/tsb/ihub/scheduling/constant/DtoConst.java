package com.tsb.ihub.scheduling.constant;

import java.util.function.Function;

import com.tsb.ihub.scheduling.bo.IHubAPIBo;
import com.tsb.ihub.scheduling.dto.IHubAPIDto;

public enum DtoConst {
	
	IHUBAPIBO_TO_EJLOG(obj-> new IHubAPIDto().createEjLogPoByIHubBo((IHubAPIBo)obj));
   
	 DtoConst(Function<Object, Object> doTranfer) {
		this.doTransfer = doTranfer;
	}
    
	private Function<Object,Object> doTransfer;

	public Function<Object, Object> getDoTransfer() {
		return doTransfer;
	}

	public void setDoTransfer(Function<Object, Object> doTransfer) {
		this.doTransfer = doTransfer;
	}
	

}
