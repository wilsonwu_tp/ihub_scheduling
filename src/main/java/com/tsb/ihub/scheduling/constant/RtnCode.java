package com.tsb.ihub.scheduling.constant;

public enum RtnCode {

	C0000("C0000","成功"),
	
	C0099("C0099","系統錯誤"),
	
	C0098("C0098","自訂錯誤訊息"),
	
	C0097("C0097","發送電文錯誤");
	
	private String rtnCode;
	private String rtnMsg;
	
	private RtnCode(String rtnCode, String rtnMsg) {
		this.rtnCode = rtnCode;
		this.rtnMsg = rtnMsg;
	}
	public String getRtnCode() {
		return rtnCode;
	}
	public void setRtnCode(String rtnCode) {
		this.rtnCode = rtnCode;
	}
	public String getRtnMsg() {
		return rtnMsg;
	}
	public void setRtnMsg(String rtnMsg) {
		this.rtnMsg = rtnMsg;
	}
	
}
