package com.tsb.ihub.scheduling.dto;

import org.springframework.beans.BeanUtils;

import com.tsb.ihub.scheduling.bo.IHubAPIBo;
import com.tsb.ihub.scheduling.po.EjLogPo;

public class IHubAPIDto {

	public EjLogPo createEjLogPoByIHubBo(IHubAPIBo obj) {
		EjLogPo ejLogPo = new EjLogPo();
		BeanUtils.copyProperties(obj, ejLogPo);
		return ejLogPo;
	}


}
